<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Color_model extends CI_Model{
	
    function __construct(){
		parent::__construct();
    }

    function get_all($offset=0,$limit=10, $order_by = 'routes_id', $sortorder = 'DESC', $param=array()){
			$db = $this->db;
			$table = "color";
			$select = !empty($param['select'])?$param['select']:'*';
			$where_clause = !empty($param['where_clause'])?$param['where_clause']:'';
			$like_clause = !empty($param['like_clause'])?$param['like_clause']:'';
			$db->distinct();
			$db->select($select)
				->from($table.' a')
				;
			(!empty($where_clause)||!empty($where_clause))?$db->where($where_clause):'';
			(!empty($like_clause)||!empty($like_clause))?$db->like($like_clause):'';
			(!empty($limit)||!empty($offset))?$db->limit($limit,$offset):'';
			$db->order_by($order_by,$sortorder);
			$q = $db->get();
			$data['results'] = $q->result();
				$db->distinct();
				$db->select('count(color_id) as total_results')
				;
			 	(!empty($where_clause)||!empty($where_clause))?$db->where($where_clause):'';
			 	(!empty($like_clause)||!empty($like_clause))?$db->like($like_clause):'';
			$q = $db->get($table .' a');
			$row = $q->row();
			$data['total_results'] = $row->total_results;
			return $data;
	}

	function add(){
	
		$data = array(
			'color_hexa'          		=> $this->input->post('color_hexa'),
			'color_value_min'          	=> $this->input->post('color_value_min'),
			'color_value_max'          	=> $this->input->post('color_value_max'),
		);
        //var_dump($data);       
		$this->db->insert('color',$data);

		return $this->db->insert_id();
	}

	function update($id){
		$data = array(
			'color_hexa'          		=> $this->input->post('color_hexa'),
			'color_value_min'          	=> $this->input->post('color_value_min'),
			'color_value_max'          	=> $this->input->post('color_value_max'),
		);
          
        $this->db->where('color_id',$id)->update('color',$data);
		return TRUE;
		
	}

	function delete($id){
		$this->db->where('color_id',$id)->delete('color');
		return TRUE;
	}

}