<div class="col-md-3 col-md-3 col-sm-3 col-xs-12  mg-tb-30">
	<form method="get" action="">
		<div class="col-md-6">
			<select class="form-control input-sm" name="year">
				<?php $i=2017;while($i <= 2019) :?>
				<option value="<?php echo $i;?>" <?php if(@$_GET['year'] == $i) echo "selected";?>><?php echo $i;?></option>
				<?php $i++;endwhile;?>
			</select>
		</div>
		<div class="col-md-6">
			<input type="submit" name="search" class="btn btn-success compose-btn btn-block m-b-md">
		</div>
	</form>
</div>
<section id="time">
	<div id="map" style="width: 100%; height: 500px;"></div>
</section>
<script type="text/javascript">
var mapMarkers = [];
<?php 
$year = @$_GET['year'];
if(empty($year)){
	$year = 2019;
}
$json = file_get_contents(base_url().'/assets/dekon.json');
//Decode JSON
$json_data = json_decode($json,true);
$total_pagu = 0;
// var_dump($json_data);
if(!empty($json_data)){
	foreach($json_data as $data){ 
		if($data['pagu'.$year] > $total_pagu){
			$total_pagu = $data['pagu'.$year];
		}


		?>

		mapMarkers['<?php echo $data['provinsi'];?>'] = {
          "no" : "<?php echo $data['no'];?>",
          "provinsi" : "<?php echo $data['provinsi'];?>",
          "pagu" : "<?php echo $data['pagu'.$year];?>"
        };

	<?
	}
}

?>
</script>
<script type="text/javascript">
	var map = L.map('map', {
      center: [-3.0093, 116.9213],
      zoom: 5,
      scrollWheelZoom: true
  	});

	L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
	}).addTo(map);

	function formatNumber(num) {
	  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
	}

	function onEachFeature(feature, layer) {
		// console.log(feature.properties.Propinsi);
		var ID = feature.properties.ID;
		var Propinsi = feature.properties.Propinsi;
		// console.log(ID);
		// console.log(mapMarkers[Propinsi]['pagu2019']);
		// var popupContent = "";
		var popupContent = "<p>" + feature.properties.Propinsi + "</p>" + "<p> Pagu <?php echo $year;?>: "+ formatNumber(mapMarkers[Propinsi]['pagu'])+"</p>";


		if (feature.properties && feature.properties.popupContent) {
			popupContent += feature.properties.popupContent;
		}

		layer.bindPopup(popupContent);
		layer.on('mouseover', function (e) {
            this.openPopup();
        });
	}

	function getColor(value){
		var color = [];
		<?php if(!empty($colors)) :?>
			<?php foreach($colors as $color):?>
				color[<?php echo $color->color_id?>] = {
					'hexa' : '<?php echo $color->color_hexa?>',
					'min' : '<?php echo $color->color_value_min?>',
					'max' : '<?php echo $color->color_value_max?>'
				}
			<?php endforeach;?>
		<?php endif;?>

		const copy = [];
		color.forEach(function(item){
			if(value > item.min && value < item.max){
		  		copy.push(item.hexa);
		  	}
		
		});

		if(copy[0]){
			return copy[0];
		}else{
			return "green";
		}

		

	}

	function funcStyle(feature) {
			var Propinsi = feature.properties.Propinsi;
			// console.log(Propinsi);
		
	        // console.log(feature);
	        // var fc = 'green';
	        var fc = getColor(mapMarkers[Propinsi]['pagu']);
		    var sc = '#f7d380';
		    var total_pagu = <?php echo $total_pagu;?>;		    
		    
		    // var opacity =  (mapMarkers[Propinsi]['pagu'] / total_pagu);
		    // if(isNaN(opacity)){
		    // 	opacity = 0;
		    // }
		    opacity = 1;

		    return {
                weight: 2,
                opacity: 1,
                color: '',
                dashArray: '3',
                fillOpacity: opacity,
                fillColor: fc
            };
	    }


	
    // Load GeoJSON.
    var geojsonLayer = new L.GeoJSON.AJAX("<?php echo base_url();?>assets/indonesia-prov.geojson.txt",{
    	style: funcStyle,
    	onEachFeature: onEachFeature
    });  

	geojsonLayer.addTo(map);
	// geojsonLayer.setStyle(myStyle);
</script>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="product-status-wrap">
            <h4>Provinsi</h4>
            <!-- <div class="add-product">
                <a href="product-edit.html">Add Product</a>
            </div> -->
            <table>
                <tr>
                    <th>No</th>
                    <th>PROVINSI</th>
                    <th>REGIONAL I <br /> GREAT CHINA</th>
                    <th>REGIONAL II <br /> ASIA PASIFIK</th>
                    <th>REGIONAL III <br />ASIA SELATAN, ASTENG, <br /> TIMTENG, AFRIKA</th>
                    <th>REGIONAL IV <br /> EROPA</th>
                    <th>PAGU 2017</th>
                    <th>PAGU 2018</th>
                    <th>PAGU 2019</th>
                </tr>
                <?php if(!empty($json_data)):?>
                	<?php foreach($json_data as $table):?>
		                <tr>
		                    <td><?php echo $table['no']?></td>
		                    <td><?php echo $table['provinsi']?></td>
		                    <td><?php if(is_numeric($table['regional1'])){echo number_format($table['regional1'], 0, ",", ".");}else{echo $table['regional1'];}?></td>
		                    <td><?php if(is_numeric($table['regional2'])){echo number_format($table['regional2'], 0, ",", ".");}else{echo $table['regional2'];}?></td>
		                    <td><?php if(is_numeric($table['regional3'])){echo number_format($table['regional3'], 0, ",", ".");}else{echo $table['regional3'];}?></td>
		                    <td><?php if(is_numeric($table['regional4'])){echo number_format($table['regional4'], 0, ",", ".");}else{echo $table['regional4'];}?></td>
		                    <td><?php if(is_numeric($table['pagu2017'])){echo number_format($table['pagu2017'], 0, ",", ".");}else{echo $table['pagu2017'];}?></td>
		                    <td><?php if(is_numeric($table['pagu2018'])){echo number_format($table['pagu2018'], 0, ",", ".");}else{echo $table['pagu2018'];}?></td>
		                    <td><?php if(is_numeric($table['pagu2019'])){echo number_format($table['pagu2019'], 0, ",", ".");}else{echo $table['pagu2019'];}?></td>
		                </tr>
		            <?php endforeach;?>
		        <?php endif;?>
            </table>
            <!-- <div class="custom-pagination">
				<ul class="pagination">
					<li class="page-item"><a class="page-link" href="#">Previous</a></li>
					<li class="page-item"><a class="page-link" href="#">1</a></li>
					<li class="page-item"><a class="page-link" href="#">2</a></li>
					<li class="page-item"><a class="page-link" href="#">3</a></li>
					<li class="page-item"><a class="page-link" href="#">Next</a></li>
				</ul>
            </div> -->
        </div>
    </div>
</div>
