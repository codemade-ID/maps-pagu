<div class="page-content-wrapper">
        <div class="product-status-wrap">
            

            <h4 class="page-title">
                <?php echo $title;?>
            </h4>

            <div class="row ">
                <div class="col-md-12 col-sm-12">
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="tools">
                                <a href="<?php echo base_url()?>color/add" class="btn "><i class="fa fa-plus"></i> Add</a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <table class="" id="videos">
                                <thead>
                                <tr>
                                    <th>
                                        ID
                                    </th>
                                    <th>
                                        Color HEXA
                                    </th>
                                    <th>
                                        Value Min
                                    </th>
                                    <th>
                                        Value Max
                                    </th>
                                    <th>
                                        Status
                                    </th>
                                </tr>
                                </thead>
                                <?php if(!empty($rows)) :?>
                                    <tbody>
                                    <?php foreach($rows as $row) :?>
                                        <tr>
                                        <td><?php echo $row->color_id?></td>
                                        <td class="center"><?php echo $row->color_hexa?></td>
                                        <td class="center"><?php echo $row->color_value_min?></td>
                                        <td class="center"><?php echo $row->color_value_max?></td>
                                        
                                        <td class="center" rel="<?=$row->color_id?>">
                                            <a class="btn btn-info" href="<?php echo base_url()?>color/edit/<?php echo $row->color_id?>/">
                                                <i class="icon-edit icon-white"></i>  
                                                Edit                                            
                                            </a>
                                            <a class="btn btn-danger delete" href="#">
                                                <i class="icon-trash icon-white"></i> 
                                                Delete
                                            </a>
                                        </td>
                                    </tr>
                                    <?php endforeach;?>
                                    </tbody>
                                <?php endif;?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

<script type="text/javascript">
    $(document).ready(function(){
        $(".delete").click(function(e){
            e.preventDefault();
            var parent = $(this).parent().parent();
            var id = $(this).parent().attr('rel');
            var msg = confirm('Are you sure you want to delete this item?');
                if(msg == true){    
                    $.ajax({
                       url: '<?php echo base_url()?>color/delete/',
                       type: "post",
                       dataType: "html",
                       data:"id="+id,
                       timeout: 2000,
                       success: function(response){
                            if(response == 'success'){
                                parent.fadeOut('slow');
                                return;
                                    //window.location.reload()
                            }else{
                                alert("Delete  failed");
                                return;
                                //window.location.reload()
                            }
                       },
                       error: function(){
                            alert('error occured on ajax request.');
                       }
                    });
                }else{
                    return;
                }
         });
    });
</script>