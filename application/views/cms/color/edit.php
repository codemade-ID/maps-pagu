<div class="page-content-wrapper">
        <div class="page-content">
            

            <h3 class="page-title">
                <?php echo $title;?>
            </h3>

            <div class="row ">
                <div class="col-md-12 col-sm-12">
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="tools">
                                <a href="/route/add" class="btn "><i class="fa fa-plus"></i> Add</a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form class="form-horizontal well" method="POST">
								<div class="control-group">
									<label class="control-label">Color HEXA <span class="f_req">*</span></label>
									<div class="controls">
										<input type="text" name="color_hexa" class="span10 inp-form form-control" value="<?=$row->color_hexa?>"/>
										<span class="help-block"><?=form_error('color_hexa');?></span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Value Min <span class="f_req">*</span></label>
									<div class="controls">
										<input type="text" name="color_value_min" class="span10 inp-form form-control" value="<?=$row->color_value_min?>"/>
										<span class="help-block"><?=form_error('color_value_min');?></span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Value Max <span class="f_req">*</span></label>
									<div class="controls">
										<input type="text" name="color_value_max" class="span10 inp-form form-control" value="<?=$row->color_value_max?>"/>
										<span class="help-block"><?=form_error('color_value_max');?></span>
									</div>
								</div>
								<div class="control-group">
									<div class="controls">
										<button class="btn btn-primary" type="submit">Save</button>
									</div>
								</div>
							
						</form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>