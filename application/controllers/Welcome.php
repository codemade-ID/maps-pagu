<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->data = array();
		
	}

	public function index()
	{
		$this->load->model('color_model');
		$data = $this->color_model->get_all(0,10,'color_id', 'DESC');
		$this->data['colors'] = $data['results'];

		$this->data['content'] = $this->load->view("home",$this->data, true);
		$this->load->view('theme_nalika', $this->data);
	}

}
