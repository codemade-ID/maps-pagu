<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Color extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("color_model");
		$this->data['title'] = "Color Page";
	}

	public function index()
	{
		$data = $this->color_model->get_all(0,100,'color_id', 'DESC');
		$this->data['rows'] = $data['results'];

		
		$this->data['content'] = $this->load->view("cms/color/index",$this->data, true);
		$this->load->view('theme_nalika', $this->data);
	}

	public function add()
	{
		$this->load->library('form_validation');
		$config = array(
			array('field' => 'color_hexa','label' => '&nbsp;<b>Color Hexa</b>','rules' =>'trim|required'),
			array('field' => 'color_value_min','label' => '&nbsp;<b>Color Value Min</b>','rules' =>'trim|required'),
			array('field' => 'color_value_max','label' => '&nbsp;<b>Color Value Max</b>','rules' =>'trim|required'),
		);
		$this->form_validation->set_message('required', '%s harus diisi.');
		$this->form_validation->set_error_delimiters('<span class="help-inline">', '</div>');
		$this->form_validation->set_rules($config);
		if($this->form_validation->run() === TRUE):
			$this->color_model->add();
			redirect(base_url().'color/index/');
		endif;

		$this->data['content'] = $this->load->view("cms/color/add",$this->data, true);
		$this->load->view('theme_nalika', $this->data);
	}

	public function edit($id)
	{
		$this->load->library('form_validation');
		$config = array(
			array('field' => 'color_hexa','label' => '&nbsp;<b>Color Hexa</b>','rules' =>'trim|required'),
			array('field' => 'color_value_min','label' => '&nbsp;<b>Color Value Min</b>','rules' =>'trim|required'),
			array('field' => 'color_value_max','label' => '&nbsp;<b>Color Value Max</b>','rules' =>'trim|required'),
		);
		$this->form_validation->set_message('required', '%s harus diisi.');
		$this->form_validation->set_error_delimiters('<span class="help-inline">', '</div>');
		$this->form_validation->set_rules($config);
		if($this->form_validation->run() === TRUE):
			$this->color_model->update($id);
			redirect(base_url().'color/index/');
		endif;

		$param['where_clause'] = array("color_id" => $id);
		$data = $this->color_model->get_all(0,1,'color_id', 'DESC',$param);
		$this->data['rows'] = $data['results'];
		$this->data['row'] = $this->data['rows'][0];

		$this->data['content'] = $this->load->view("cms/color/edit",$this->data, true);
		$this->load->view('theme_nalika', $this->data);
	}

	function delete(){
		if($_POST):
			$this->color_model->delete($this->input->post('id'));
			echo "success";
			exit;
		endif;
	}
}